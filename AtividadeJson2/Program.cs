﻿using AtividadeJson2.Models2;
using Newtonsoft.Json;
using System;
using System.Net;

namespace AtividadeJson2
{
    class Program
    {
        static void Main(string[] args)
        {

            string url = "http://senacao.tk/objetos/computador_array_objeto";

            Computador computador = BuscarComputador(url);

            Console.WriteLine("Marca: {0} - Modelo: {1} - Memoria {2} Processador: {3}"
                , computador.Marca, computador.Modelo, computador.Memoria, computador.Precessador);

            Console.WriteLine("\nSoftwares:");

            foreach (string sf in computador.Softwares)
            {
                Console.WriteLine("-" + sf);
            }
          
            Console.WriteLine("\nDados do Fornecedor\n");
            Console.WriteLine("- " + computador.Fornecedor.RSocial);
            Console.WriteLine("- " + computador.Fornecedor.Telefone);
            Console.WriteLine("- " + computador.Fornecedor.Endereco);

            Console.ReadLine();





        }

        public static Computador BuscarComputador(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            Computador computador = JsonConvert.DeserializeObject<Computador>(content);

            return computador;

        }
    }
}
