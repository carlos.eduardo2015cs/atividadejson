﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtividadeJson2.Models2
{
    class Computador
    {

        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Memoria { get; set; }
        public int SSD { get; set; }
        public string Precessador { get; set; }

        public List<string> Softwares { get; set; }

        public Fornecedor Fornecedor { get; set; }

        public string RSocial { get; set; }
        public int Telefone { get; set; }
        public string Endereco { get; set; }

        public List<lojas> lojas { get; set; }
    }
     class Fornecedor
     {
        public string RSocial { get; set; }
        public int Telefone { get; set; }
        public string Endereco { get; set; }



     }
    class lojas
    {
        public string Cidade { get; set; }
        public string endereco { get; set; }
        public string tel { get; set; }

        public List<string> loja { get; set; }

    }





}

