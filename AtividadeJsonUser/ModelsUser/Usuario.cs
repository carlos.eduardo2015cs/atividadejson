﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtividadeJsonUser.ModelsUser
{ 
   

       public class Usuario
       {
            public string Nome { get; set; }
            public string Email { get; set; }
            public int Telefone { get; set; }
            public List<string> Conhecimentos { get; set; }
            public Endereco Endereco { get; set; }
            public List<Qualificacao> Qualificacoes { get; set; }
       }
       public class Endereco
       { 

            public string Rua { get; set; }
            public int Numero { get; set; }
            public string Bairro { get; set; }
            public string Cidade { get; set; }
            public string Uf { get; set; }
       }
        

        

       public  class Qualificacao
       {
            public string Nome { get; set; }
            public string Instituicao { get; set; }
            public int Ano { get; set; }
       }

     






}

