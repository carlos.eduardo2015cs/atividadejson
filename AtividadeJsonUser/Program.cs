﻿using AtividadeJsonUser.ModelsUser;
using Newtonsoft.Json;
using System;
using System.Net;

namespace AtividadeJsonUser
{
    class Program
    {
        static void Main(string[] args)
        {
            string user = "http://senacao.tk/objetos/usuario";

            Usuario usuario = BuscarUsuario(user);

            Console.WriteLine("");
            Console.WriteLine(string.Format("Nome: {0} - Email: {1} - Telefone: {2}"
                , usuario.Nome, usuario.Email, usuario.Telefone));

            foreach (string op in usuario.Conhecimentos)
            {
                Console.WriteLine("- " + op);


            }
            Console.WriteLine("\nEndereço do Usuario\n");
            Console.WriteLine("Rua- " + usuario.Endereco.Rua);
            Console.WriteLine("Bairro- " + usuario.Endereco.Bairro);
            Console.WriteLine("Numero- " + usuario.Endereco.Numero);
            Console.WriteLine("Cidade- " + usuario.Endereco.Cidade);
            Console.WriteLine("UF- " + usuario.Endereco.Uf);
            Console.WriteLine("");


            foreach (Qualificacao q in usuario.Qualificacoes)
            {
                Console.WriteLine("Nome- " + q.Nome);
                Console.WriteLine("Ano- " + q.Ano);
                Console.WriteLine("Instituição- " + q.Instituicao);
                Console.WriteLine("");

            }




            Console.ReadLine();


        }
        public static Usuario BuscarUsuario(string user)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(user);
            Console.WriteLine(content);
            Console.WriteLine("\n\n");
            Usuario usuario = JsonConvert.DeserializeObject<Usuario>(content);
            return usuario;
        }

    }
}
