﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AtvidadeJson.Models
{
    public class Veiculo
    {
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Ano { get; set; }
        public int Quilometragem { get; set; }

        public List<string> opcionais { get; set; }

        public vendedor vendedor { get; set; }

        public List<revisoes> revisoes { get; set; }



    }
    public class vendedor
    {
        public string nome { get; set; }
        public int idade { get; set; }
        public int celular { get; set; }
        public string cidade { get; set; }
        public revisoes revisoes{ get; set; }
    }
    public class revisoes
    {
        public float Data { get; set; }
        public int km { get; set; }
        public string oficina { get; set; }
    }
}
