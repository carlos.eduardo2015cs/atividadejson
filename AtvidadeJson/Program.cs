﻿using AtvidadeJson.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace AtvidadeJson
{
    public class Program
    {
       //public class Computador
       // {
       //     public string Marca { get; set; }
       //     public string Modelo { get; set; }
       //     public int Ano { get; set; }
       //     public int Quilometragem { get; set; }
       // }

        static void Main(string[]args)
        {
            string url = "http://senacao.tk/objetos/veiculo_array_objeto";

            Veiculo veiculo = BuscarVeiculo(url);
          


            Console.WriteLine("");
            Console.WriteLine(string.Format("Marca: {0} - Modelo: {1} - Ano: {2} - Quilometragem: {3}"
                , veiculo.Marca, veiculo.Modelo, veiculo.Ano, veiculo.Quilometragem));
            Console.WriteLine("\nOpcionais:");

            foreach(string op in veiculo.opcionais)
            {
                Console.WriteLine("-" + op);
            }
            Console.WriteLine("\nDados do vendedor\n");
            Console.WriteLine("- " + veiculo.vendedor.nome);
            Console.WriteLine("- " + veiculo.vendedor.cidade);
            Console.WriteLine("- " + veiculo.vendedor.idade);
            Console.WriteLine("- " + veiculo.vendedor.celular); 




            Console.ReadLine();




        }
        public static Veiculo BuscarVeiculo(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            Veiculo veiculo = JsonConvert.DeserializeObject<Veiculo>(content);

            return veiculo;


        }
    }
}
