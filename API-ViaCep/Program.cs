﻿using Newtonsoft.Json;
using System;
using System.Net;
using static API_ViaCep.ModelsAPI.VIACEP;

namespace API_ViaCep
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Qual Endereço deseja?");
            string cep = "";
            
            cep = Console.ReadLine();

            string url = "https://viacep.com.br/ws/"+cep+"/json/";

            
            Endereco Endereco = BuscarEndereco(url);
            Console.WriteLine("");
            Console.WriteLine(string.Format(Endereco.Cep, Endereco.Logradouro, Endereco.Complemento, Endereco.Bairro, Endereco.Localidade,
            Endereco.UF, Endereco.Unidade, Endereco.Ibge, Endereco.Gia));


            Console.WriteLine("\nEndereço\n");
            Console.WriteLine("Endereço- " + Endereco.Cep);
            Console.WriteLine("Logradouro- " + Endereco.Logradouro);
            Console.WriteLine("Complemento- " + Endereco.Complemento);
            Console.WriteLine("Bairro- " + Endereco.Bairro);
            Console.WriteLine("Localidade- " + Endereco.Localidade);
            Console.WriteLine("UF- " + Endereco.UF);
            Console.WriteLine("Unidade- " + Endereco.Unidade);
            Console.WriteLine("Ibge- " + Endereco.Ibge);
            Console.WriteLine("Gia- " + Endereco.Gia);

            Console.WriteLine("");
            Console.ReadLine();






        }

        public static Endereco BuscarEndereco(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            Console.WriteLine(content);
            Console.WriteLine("\n\n");
            Endereco Endereco = JsonConvert.DeserializeObject<Endereco>(content);
            return Endereco;
        }
    }

    
}
