﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_ViaCep.ModelsAPI
{
    class VIACEP
    {

        public class Endereco
        {
            public string Cep { get; set; }
            public string Logradouro { get; set; }
            public string Complemento { get; set; }
            public string Bairro { get; set; }
            public string Localidade { get; set; }
            public string UF { get; set; }
            public string Unidade { get; set; }
            public int Ibge { get; set; }
            public int Gia { get; set; }
        }












    }
}
