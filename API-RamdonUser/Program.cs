﻿using API_RamdonUser.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace API_RamdonUser
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "https://randomuser.me/api/";

            RamdonUser RandomUser = SearchUser(url);

            Console.WriteLine("");

            foreach(Result user in RandomUser.Results  )
            {
                Console.WriteLine("\n====== Usuario ======\n");
                Console.WriteLine("Nome: " + user.Name.First);
                Console.WriteLine("Sobenome: " + user.Name.Last);
                Console.WriteLine("Titulo: " + user.Name.Title);
                Console.WriteLine("Sexo: " + user.Gender);
                Console.WriteLine("E-mail: " + user.Email);

                Console.WriteLine("\n====== Localização ======\n");
                Console.WriteLine("Rua: " + user.Location.Street.Name, 
                    " Nº " + user.Location.Street.Number);
                Console.WriteLine("Cidade: " + user.Location.City);
                Console.WriteLine("Estado: " + user.Location.State);
                Console.WriteLine("Pais: " + user.Location.Country);
                Console.WriteLine("Codigo Postal: " + user.Location.Postcode);

                Console.WriteLine("\nCoordernadas");
                Console.WriteLine("Latitude: " + user.Location.Coordinates.
                    Latitude);
                Console.WriteLine("Longitude: " + user.Location.Coordinates.
                    Longitude);

                Console.WriteLine("\nTimezone");
                Console.WriteLine("Offset: " + user.Location.Timezone
                    .Offset);
                Console.WriteLine("Description: " + user.Location.Timezone.
                    Description);

                
            }
            Console.ReadLine();
        }

        public static RamdonUser SearchUser(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);
            RamdonUser RamdonUser = JsonConvert
                .DeserializeObject<RamdonUser>(content);

            Console.WriteLine(content);
            Console.WriteLine("\n");
            return RamdonUser;
        }
    }
}
