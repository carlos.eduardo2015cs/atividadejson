﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API_RamdonUser.Models
{
    class RamdonUser
    {
        public List<Result> Results { get; set; }

    
    }
    class Result
    {
       
        public Name Name { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public Location Location { get; set; }
    }
    class Name
    {
        
        
        public string Title { get; set; }
        public string First { get; set; }
        public string Last { get; set; }
    }
    class Location
    {
        public Street Street { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
         public string State { get; set; }
        public string Postcode { get; set; }
        public Coordinates Coordinates { get; set; }
        public Timezone Timezone { get; set; }

    }
    class Street
    {
        public int Number { get; set; }
        public string Name { get; set; }

    }
    class Coordinates
    {
        public string Latitude { get; set; }

        public string Longitude { get; set; }
    }
   class Timezone
    {
        public string Offset { get; set; }
        public string Description { get; set; }
    }
   


}
